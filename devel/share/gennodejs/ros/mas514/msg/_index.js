
"use strict";

let ServoSetpoints = require('./ServoSetpoints.js');
let WebJoystick = require('./WebJoystick.js');

module.exports = {
  ServoSetpoints: ServoSetpoints,
  WebJoystick: WebJoystick,
};
