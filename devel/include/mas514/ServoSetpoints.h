// Generated by gencpp from file mas514/ServoSetpoints.msg
// DO NOT EDIT!


#ifndef MAS514_MESSAGE_SERVOSETPOINTS_H
#define MAS514_MESSAGE_SERVOSETPOINTS_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace mas514
{
template <class ContainerAllocator>
struct ServoSetpoints_
{
  typedef ServoSetpoints_<ContainerAllocator> Type;

  ServoSetpoints_()
    : rightWheel(0)
    , leftWheel(0)
    , servo1(0)
    , servo2(0)
    , servo3(0)  {
    }
  ServoSetpoints_(const ContainerAllocator& _alloc)
    : rightWheel(0)
    , leftWheel(0)
    , servo1(0)
    , servo2(0)
    , servo3(0)  {
  (void)_alloc;
    }



   typedef int64_t _rightWheel_type;
  _rightWheel_type rightWheel;

   typedef int64_t _leftWheel_type;
  _leftWheel_type leftWheel;

   typedef int64_t _servo1_type;
  _servo1_type servo1;

   typedef int64_t _servo2_type;
  _servo2_type servo2;

   typedef int64_t _servo3_type;
  _servo3_type servo3;





  typedef boost::shared_ptr< ::mas514::ServoSetpoints_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::mas514::ServoSetpoints_<ContainerAllocator> const> ConstPtr;

}; // struct ServoSetpoints_

typedef ::mas514::ServoSetpoints_<std::allocator<void> > ServoSetpoints;

typedef boost::shared_ptr< ::mas514::ServoSetpoints > ServoSetpointsPtr;
typedef boost::shared_ptr< ::mas514::ServoSetpoints const> ServoSetpointsConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::mas514::ServoSetpoints_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::mas514::ServoSetpoints_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::mas514::ServoSetpoints_<ContainerAllocator1> & lhs, const ::mas514::ServoSetpoints_<ContainerAllocator2> & rhs)
{
  return lhs.rightWheel == rhs.rightWheel &&
    lhs.leftWheel == rhs.leftWheel &&
    lhs.servo1 == rhs.servo1 &&
    lhs.servo2 == rhs.servo2 &&
    lhs.servo3 == rhs.servo3;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::mas514::ServoSetpoints_<ContainerAllocator1> & lhs, const ::mas514::ServoSetpoints_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace mas514

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsFixedSize< ::mas514::ServoSetpoints_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::mas514::ServoSetpoints_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::mas514::ServoSetpoints_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::mas514::ServoSetpoints_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::mas514::ServoSetpoints_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::mas514::ServoSetpoints_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::mas514::ServoSetpoints_<ContainerAllocator> >
{
  static const char* value()
  {
    return "790ff8777d41a4ebf31d8435d32b565b";
  }

  static const char* value(const ::mas514::ServoSetpoints_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x790ff8777d41a4ebULL;
  static const uint64_t static_value2 = 0xf31d8435d32b565bULL;
};

template<class ContainerAllocator>
struct DataType< ::mas514::ServoSetpoints_<ContainerAllocator> >
{
  static const char* value()
  {
    return "mas514/ServoSetpoints";
  }

  static const char* value(const ::mas514::ServoSetpoints_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::mas514::ServoSetpoints_<ContainerAllocator> >
{
  static const char* value()
  {
    return "int64 rightWheel\n"
"int64 leftWheel\n"
"int64 servo1\n"
"int64 servo2\n"
"int64 servo3\n"
;
  }

  static const char* value(const ::mas514::ServoSetpoints_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::mas514::ServoSetpoints_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.rightWheel);
      stream.next(m.leftWheel);
      stream.next(m.servo1);
      stream.next(m.servo2);
      stream.next(m.servo3);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct ServoSetpoints_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::mas514::ServoSetpoints_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::mas514::ServoSetpoints_<ContainerAllocator>& v)
  {
    s << indent << "rightWheel: ";
    Printer<int64_t>::stream(s, indent + "  ", v.rightWheel);
    s << indent << "leftWheel: ";
    Printer<int64_t>::stream(s, indent + "  ", v.leftWheel);
    s << indent << "servo1: ";
    Printer<int64_t>::stream(s, indent + "  ", v.servo1);
    s << indent << "servo2: ";
    Printer<int64_t>::stream(s, indent + "  ", v.servo2);
    s << indent << "servo3: ";
    Printer<int64_t>::stream(s, indent + "  ", v.servo3);
  }
};

} // namespace message_operations
} // namespace ros

#endif // MAS514_MESSAGE_SERVOSETPOINTS_H
