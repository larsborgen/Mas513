#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/jetbot/mmwave_ti_ros/ros_driver/src/rosserial_python"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/jetbot/mmwave_ti_ros/ros_driver/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/jetbot/mmwave_ti_ros/ros_driver/install/lib/python2.7/dist-packages:/home/jetbot/mmwave_ti_ros/ros_driver/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/jetbot/mmwave_ti_ros/ros_driver/build" \
    "/usr/bin/python2" \
    "/home/jetbot/mmwave_ti_ros/ros_driver/src/rosserial_python/setup.py" \
     \
    build --build-base "/home/jetbot/mmwave_ti_ros/ros_driver/build/rosserial_python" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/jetbot/mmwave_ti_ros/ros_driver/install" --install-scripts="/home/jetbot/mmwave_ti_ros/ros_driver/install/bin"
