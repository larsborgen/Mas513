# CMake generated Testfile for 
# Source directory: /home/jetbot/mmwave_ti_ros/ros_driver/src
# Build directory: /home/jetbot/mmwave_ti_ros/ros_driver/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("rosserial_python")
subdirs("rosserial_client")
subdirs("serial")
subdirs("ti_mmwave_rospkg")
subdirs("mas514")
